package br.com.mastertech.trace.client;

import br.com.mastertech.trace.dto.CEPDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "consulta-cep")
public interface ConsultaCEPClient {

    @GetMapping("/cep/{cep}")
    Optional<CEPDTO> findByCEP(@PathVariable(value = "cep") String cep);
}
