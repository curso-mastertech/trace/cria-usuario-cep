package br.com.mastertech.trace.dto;

import javax.validation.constraints.NotBlank;

public class Usuario {

    @NotBlank
    private String nome;
    private CEPDTO cep;

    public String getNome() {
        return nome;
    }

    public Usuario setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public CEPDTO getCep() {
        return cep;
    }

    public Usuario setCep(CEPDTO cep) {
        this.cep = cep;
        return this;
    }
}
