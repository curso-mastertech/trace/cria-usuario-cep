package br.com.mastertech.trace.dto;

public class CEPDTO {

    private String cep;
    private String logradouro;
    private String complemento;
    private String bairro;
    private String localidade;
    private String uf;
    private String unidade;
    private String ibge;
    private String gia;

    public String getCep() {
        return cep;
    }

    public CEPDTO setCep(String cep) {
        this.cep = cep;
        return this;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public CEPDTO setLogradouro(String logradouro) {
        this.logradouro = logradouro;
        return this;
    }

    public String getComplemento() {
        return complemento;
    }

    public CEPDTO setComplemento(String complemento) {
        this.complemento = complemento;
        return this;
    }

    public String getBairro() {
        return bairro;
    }

    public CEPDTO setBairro(String bairro) {
        this.bairro = bairro;
        return this;
    }

    public String getLocalidade() {
        return localidade;
    }

    public CEPDTO setLocalidade(String localidade) {
        this.localidade = localidade;
        return this;
    }

    public String getUf() {
        return uf;
    }

    public CEPDTO setUf(String uf) {
        this.uf = uf;
        return this;
    }

    public String getUnidade() {
        return unidade;
    }

    public CEPDTO setUnidade(String unidade) {
        this.unidade = unidade;
        return this;
    }

    public String getIbge() {
        return ibge;
    }

    public CEPDTO setIbge(String ibge) {
        this.ibge = ibge;
        return this;
    }

    public String getGia() {
        return gia;
    }

    public CEPDTO setGia(String gia) {
        this.gia = gia;
        return this;
    }
}
