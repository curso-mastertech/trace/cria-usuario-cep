package br.com.mastertech.trace.controller;

import br.com.mastertech.trace.dto.Usuario;
import br.com.mastertech.trace.service.CriaUsuarioCEPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuario")
public class UsuarioCEPController {

    @Autowired
    private CriaUsuarioCEPService criaUsuarioService;

    @PostMapping("/cep/{cep}")
    public Usuario criaUsuario(@Valid @RequestBody Usuario usuario, @PathVariable String cep) {
        return criaUsuarioService.criaUsuario(cep, usuario);
    }
}
